## yggio-account: A 'login service' for yggio services

#### Note: this replaces 'yggio-service-account-manager'

to register a new service provider, an yggio-user is needed. This module encapsulates
the stuff necessary to use "user information" to login/register that yggio-user.

### Usage

```javascript
const account = require('yggio-account');
// you need a config with the correct stuffs
const serviceAccountInfo = {
  username: 'my-new-service@test.com',
  password: 'so-secret-so-secret'
  retryDelay: 7000 // ms (if !retryDelay, default is 5000 ms)
};
return serviceAccountManager.connect(serviceAccountInfo)
  .then(serviceAccount => {
    // typically: create/retrieve the service provider now that we
    // have our serviceAccount. And other setup stuff.
    // For now serviceAccount = {accessToken} (set by yggio-config, so the value of this key is configurable)
  });
```
### License
yggio-account is under the MIT license. See LICENSE file.
