'use strict';

const get = require('lodash.get');
const delay = require('delay');
const yggioApi = require('yggio-api');

const recursiveLogin = (serviceAccountInfo) => {
  return yggioApi.register(serviceAccountInfo)
    .catch(err => {
      console.warn('yggio-subscriber: failed to register, trying to login instead');
      return yggioApi.login(serviceAccountInfo);
    })
    .then(serviceAccount => {
      const account = Object.assign({}, serviceAccountInfo, serviceAccount);
      console.info('Register/Login account: ', get(account, 'username'));
      return account;
    })
    .catch(err => {
      const retryDelay = get(serviceAccountInfo, 'retryDelay', 5000);
      console.warn('Register/Login failed: ' + err.message + '. Retrying in ' + retryDelay + ' ms');
      return delay(retryDelay)
        .then(() => recursiveLogin(serviceAccountInfo));
    });
};

module.exports = {
  connect: recursiveLogin
};
